# Core Wars library

[ ![Download](https://api.bintray.com/packages/st-praktikum16-g1/corewars-library/corewars-lib/images/download.svg) ](https://bintray.com/st-praktikum16-g1/corewars-library/corewars-lib/_latestVersion) 
[![build status](https://gitlab.com/st-praktikum16-g1/corewars-lib/badges/master/build.svg)](https://gitlab.com/st-praktikum16-g1/corewars-lib/commits/master)
[![coverage report](https://gitlab.com/st-praktikum16-g1/corewars-lib/badges/master/coverage.svg)](https://gitlab.com/st-praktikum16-g1/corewars-lib/commits/master)


This is a library which contains the common code of the [Core Wars server](https://gitlab.com/st-praktikum16-g1/corewars-server) and [client](https://gitlab.com/st-praktikum16-g1/corewars-client).
