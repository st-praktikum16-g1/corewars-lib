package gone.lib.network.test;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Awaitility.fieldIn;
import static org.junit.Assert.assertTrue;

import gone.lib.common.ClientRole;
import gone.lib.network.contracts.ITcpListener;
import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwMessageType;
import gone.lib.network.serialization.IncomingMessagesInterpreter;
import gone.lib.network.serialization.OutgoingMessagesSerializer;
import gone.lib.network.tcp.TcpClientModule;
import gone.lib.network.tcp.TcpServerModule;

import org.junit.Test;

import java.util.concurrent.Callable;

/**
 * Created by winfr on 26.09.2016.
 */
public class ClientLoginTest {
    class ServerListener implements ITcpListener {
        public ServerListener() {
            receivedLogin = false;
        }

        @Override
        public void receiveMessage(String sessionId, String data) {
            IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
            receivedLogin = interpreter.getMessageType(data) == CwMessageType.Login;
        }

        private boolean receivedLogin;
    }

    @Test
    public void testSingleClientLogin() throws InterruptedException {
        TcpServerModule server = new TcpServerModule();
        ServerListener listener = new ServerListener();
        server.registerListener(listener);
        server.start(5557);

        TcpClientModule client = new TcpClientModule();
        client.connect("localhost", 5557);
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();
        CwLoginContent content = new CwLoginContent();
        content.name = "testWarrior";
        content.role = ClientRole.Player;
        client.sendMessage(serializer.serializeLoginMessage(content));

        Callable<Boolean> hasReceivedLogin = fieldIn(listener).ofType(boolean.class).andWithName("receivedLogin");
        await().atMost(1, SECONDS).until(hasReceivedLogin);
        server.stop();
        client.disconnect();

        assertTrue(listener.receivedLogin);
    }
}
