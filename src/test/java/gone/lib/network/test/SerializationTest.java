package gone.lib.network.test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import gone.lib.common.ClientRole;
import gone.lib.common.RcStandard;
import gone.lib.network.json.*;
import gone.lib.network.serialization.IncomingMessagesInterpreter;
import gone.lib.network.serialization.OutgoingMessagesSerializer;

import org.junit.Test;

import java.util.LinkedList;

/**
 * Created by winfr on 27.09.2016.
 */
public class SerializationTest {
    @Test
    public void testGameStatusMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwGameStatusContent content = new CwGameStatusContent();

        content.playerOneActive = true;
        content.oldIpIndex = -1;
        content.newIpIndex = 1;
        content.indexOfChangedCoreElement = 10;
        content.newOpCode = "MOV";

        String parsedMessage = serializer.serializeGameStatusMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.GameStatus));
        CwGameStatusContent newContent = interpreter.parseGameStatusMessage(parsedMessage);

        assertThat(content.indexOfChangedCoreElement, is(newContent.indexOfChangedCoreElement));
        assertThat(content.newIpIndex, is(newContent.newIpIndex));
        assertThat(content.oldIpIndex, is(newContent.oldIpIndex));
        assertThat(content.newOpCode, is(newContent.newOpCode));
        assertThat(content.playerOneActive, is(newContent.playerOneActive));
    }

    @Test
    public void testLoginMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwLoginContent content = new CwLoginContent();

        content.role = ClientRole.Player;
        content.name = "Name";

        String parsedMessage = serializer.serializeLoginMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.Login));
        CwLoginContent newContent = interpreter.parseLoginMessage(parsedMessage);

        assertThat(content.name, is(newContent.name));
        assertThat(content.role, is(newContent.role));
    }

    @Test
    public void testLoginOkMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwLoginOkContent content = new CwLoginOkContent();

        content.lineLength = 100;
        content.coreSize = 500;
        content.clientMustWait = false;
        content.standard = RcStandard.ICWS94;

        String parsedMessage = serializer.serializeLoginOkMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.LoginOk));
        CwLoginOkContent newContent = interpreter.parseLoginOkMessage(parsedMessage);

        assertThat(content.lineLength, is(newContent.lineLength));
        assertThat(content.coreSize, is(newContent.coreSize));
        assertThat(content.clientMustWait, is(newContent.clientMustWait));
    }

    @Test
    public void testReadyMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwReadyContent content = new CwReadyContent();

        content.warriorRedcode = new LinkedList<>();
        content.warriorRedcode.add("MOV #4 4");
        content.warriorRedcode.add("ADD 1 1");
        content.warriorRedcode.add("SUB 0 0");

        String parsedMessage = serializer.serializeReadyMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.Ready));
        CwReadyContent newContent = interpreter.parseReadyMessage(parsedMessage);

        assertThat(newContent.warriorRedcode.size(), is(content.warriorRedcode.size()));
        assertThat(newContent.warriorRedcode.get(0), is(content.warriorRedcode.get(0)));
        assertThat(newContent.warriorRedcode.get(1), is(content.warriorRedcode.get(1)));
        assertThat(newContent.warriorRedcode.get(2), is(content.warriorRedcode.get(2)));
    }

    @Test
    public void testStartMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwStartContent content = new CwStartContent();

        content.players = 100;
        content.fullState = new LinkedList<>();

        content.fullState.add("CORE_DAT");
        content.fullState.add("USER_DAT");
        content.fullState.add("MOV");
        content.fullState.add("ADD");

        String parsedMessage = serializer.serializeStartMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.Start));
        CwStartContent newContent = interpreter.parseStartMessage(parsedMessage);

        assertThat(content.players, is(newContent.players));
        assertThat(content.fullState, is(newContent.fullState));
        assertThat(newContent.fullState.size(), is(content.fullState.size()));

        assertThat(newContent.fullState.get(0), is(content.fullState.get(0)));
        assertThat(newContent.fullState.get(2), is(content.fullState.get(2)));
        assertThat(newContent.fullState.get(3), is(content.fullState.get(3)));
        assertThat(newContent.fullState.get(1), is(content.fullState.get(1)));
    }

    @Test
    public void testGameResultMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwGameResultContent content = new CwGameResultContent();
        content.state = "draw";

        String parsedMessage = serializer.serializeGameResultMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.GameResult));
        CwGameResultContent newContent = interpreter.parseGameResultMessage(parsedMessage);

        assertThat(content.state, is(newContent.state));
    }

    @Test
    public void testGameStatusNullMessage() {
        IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();
        OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();

        CwGameStatusContent content = new CwGameStatusContent();

        content.playerOneActive = true;
        content.oldIpIndex = -1;
        content.newIpIndex = 1;
        content.indexOfChangedCoreElement = -1;
        content.newOpCode = null;

        String parsedMessage = serializer.serializeGameStatusMessage(content);

        assertThat(interpreter.getMessageType(parsedMessage), is(CwMessageType.GameStatus));
        CwGameStatusContent newContent = interpreter.parseGameStatusMessage(parsedMessage);

        assertThat(content.indexOfChangedCoreElement, is(newContent.indexOfChangedCoreElement));
        assertThat(content.newIpIndex, is(newContent.newIpIndex));
        assertThat(content.oldIpIndex, is(newContent.oldIpIndex));
        assertThat(content.newOpCode, is(newContent.newOpCode));
        assertThat(content.playerOneActive, is(newContent.playerOneActive));
    }
}