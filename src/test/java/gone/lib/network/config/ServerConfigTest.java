package gone.lib.network.config;

import gone.lib.config.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;

import static org.junit.Assert.assertEquals;

/**
 * @author matthias
 */
public class ServerConfigTest {
    private SecureRandom random;
    private ServerConfig defaultCfg;
    private String defaultCfgS;

    // help function to generate random strings (file name)
    private String nextRandomString() {
        return new BigInteger(130, random).toString(32);
    }

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void saveFilesToHome() throws IOException {
        random = new SecureRandom();

        File defaultConfig = folder.newFile(nextRandomString() + ".xml");
        defaultCfgS = defaultConfig.getAbsolutePath();

        // create config params objects
        defaultCfg = new ServerConfig(3000, 1000, 1337, 2, 8000, 100);

        // save config files
        new SaveConfigFile(defaultCfg, defaultCfgS, ConfigTypes.SERVER_CONFIG);
    }

    @Test
    public void testCfgFileLoader_defaultClientConfig() throws IOException,
            InvalidPreferencesFormatException,
            IllegalAccessException, BackingStoreException {

        LoadConfigFile loadCfg = new LoadConfigFile();
        loadCfg.loadServerPrefs(defaultCfgS, ConfigTypes.SERVER_CONFIG);
        ServerConfig loadCfgParams = (ServerConfig) loadCfg.getConfigParameters();

        assertEquals(3000, loadCfgParams.getMaxCycles());
        assertEquals(1000, loadCfgParams.getMaxStartInstructions());
        assertEquals(1337, loadCfgParams.getPort());
        assertEquals(2, loadCfgParams.getMaxPlayersCount());
        assertEquals(8000, loadCfgParams.getCoreSize());
        assertEquals(100, loadCfgParams.getWaitTimeMs());
    }

    @Test
    public void testSetMaxCycles_Error() {
        thrown.expect(IllegalArgumentException.class);
        new ServerConfig(-5, 1000, 1337, 2, 8000, 100);
    }

    @Test
    public void testSetMaxStartInstr_Error() {
        thrown.expect(IllegalArgumentException.class);
        new ServerConfig(3000, 0, 1337, 2, 8000, 100);
    }

    @Test
    public void testSetPort_Error() {
        thrown.expect(IllegalArgumentException.class);
        new ServerConfig(3000, 1000, -1, 2, 8000, 100);
    }

    @Test
    public void testSetMaxPlayers_Error() {
        thrown.expect(IllegalArgumentException.class);
        new ServerConfig(3000, 1000, 1337, 0, 8000, 100);
    }

    @Test
    public void testSetCoreSize_Error() {
        thrown.expect(IllegalArgumentException.class);
        new ServerConfig(3000, 1000, 1337, 2, 0, 100);
    }

    @Test
    public void testSetWaitTime_Error() {
        thrown.expect(IllegalArgumentException.class);
        new ServerConfig(3000, 1000, 1337, 2, 8000, 0);
    }

    @Test
    public void saveServerConfig_EmptyPath() throws IOException {
        thrown.expect(IllegalArgumentException.class);
        ServerConfig params = new ServerConfig(3000, 1000, 1337, 2, 8000, 100);
        new SaveConfigFile(params, "", ConfigTypes.SERVER_CONFIG);
    }
}
