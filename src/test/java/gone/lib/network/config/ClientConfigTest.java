package gone.lib.network.config;

import static org.junit.Assert.assertEquals;

import gone.lib.common.Language;
import gone.lib.common.RcStandard;
import gone.lib.config.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;

/**
 *
 * @author matthias
 */
public class ClientConfigTest {

    // TODO: help function for less code redundance

    private ClientConfig defaultCfg;
    private ClientConfig userCfg;
    private ClientConfig berndCfg;
    private String defaultCfgS;
    private String userCfgS;
    private String berndCfgS;
    private SecureRandom random;

    // help function to generate random strings (file name)
    private String nextRandomString() {
        return new BigInteger(130, random).toString(32);
    }

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void saveFilesToHome() throws IOException {
        random = new SecureRandom();

        File defaultConfig = folder.newFile(nextRandomString() + ".xml");
        defaultCfgS = defaultConfig.getAbsolutePath();
        File userConfig = folder.newFile(nextRandomString() + ".xml");
        userCfgS = userConfig.getAbsolutePath();
        File berndConfig = folder.newFile(nextRandomString() + ".xml");
        berndCfgS = berndConfig.getAbsolutePath();

        // create config params objects
        defaultCfg = new ClientConfig(1024, 800, "default", System.getProperty("user.home"),
                InetAddress.getByName("222.111.222.1"), 65501, RcStandard.ICWS88, Language.en);
        userCfg = new ClientConfig(1024, 800, "user", System.getProperty("user.home"),
                InetAddress.getByName("212.121.212.121"), 64321, RcStandard.ICWS94, Language.de);
        berndCfg = new ClientConfig(1920, 1080, "Bernd Wiesacher", System.getProperty("user.home"),
                InetAddress.getByName("188.68.54.64"), 65432, RcStandard.ICWS88, Language.en);

        // save 3 config files
        new SaveConfigFile(defaultCfg, defaultCfgS, ConfigTypes.CLIENT_CONFIG);
        new SaveConfigFile(userCfg, userCfgS, ConfigTypes.CLIENT_CONFIG);
        new SaveConfigFile(berndCfg, berndCfgS, ConfigTypes.CLIENT_CONFIG);
    }

    @Test
    public void testCfgFileLoader_defaultClientConfig() throws IOException,
            InvalidPreferencesFormatException,
            IllegalAccessException, BackingStoreException {
        LoadConfigFile loadCfg = new LoadConfigFile();
        loadCfg.loadClientPrefs(defaultCfgS, defaultCfgS, ConfigTypes.CLIENT_CONFIG);
        ClientConfig loadCfgParams = (ClientConfig) loadCfg.getConfigParameters();

        assertEquals(1024, loadCfgParams.getResolutionWidth());
        assertEquals(800, loadCfgParams.getResolutionHeight());
        assertEquals("default", loadCfgParams.getWarriorName());
        assertEquals(System.getProperty("user.home"), loadCfgParams.getPathToRcFile());
        assertEquals("222.111.222.1", loadCfgParams.getInetAddress().getHostAddress());
        assertEquals(65501, loadCfgParams.getPortLastServer());
        assertEquals("ICWS88", loadCfgParams.getRcStandard().toString());
        assertEquals("en", loadCfgParams.getLanguage().toString());
    }

    @Test
    public void testCfgFileLoader_userClientConfig() throws IOException,
            InvalidPreferencesFormatException,
            IllegalAccessException, BackingStoreException {
        LoadConfigFile loadCfg = new LoadConfigFile();
        loadCfg.loadClientPrefs(defaultCfgS, userCfgS, ConfigTypes.CLIENT_CONFIG);
        ClientConfig loadCfgParams = (ClientConfig) loadCfg.getConfigParameters();

        assertEquals(1024, loadCfgParams.getResolutionWidth());
        assertEquals(800, loadCfgParams.getResolutionHeight());
        assertEquals("user", loadCfgParams.getWarriorName());
        assertEquals(System.getProperty("user.home"), loadCfgParams.getPathToRcFile());
        assertEquals("212.121.212.121", loadCfgParams.getInetAddress().getHostAddress());
        assertEquals(64321, loadCfgParams.getPortLastServer());
        assertEquals("ICWS94", loadCfgParams.getRcStandard().toString());
        assertEquals("en", loadCfgParams.getLanguage().toString());
    }

    @Test
    public void testCfgFileSaver_userClientConfig() throws IOException, BackingStoreException,
            InvalidPreferencesFormatException, IllegalAccessException {
        // save user config
        ClientConfig saveParams = new ClientConfig(1920, 1080, "Bernd Wiesacher",
                "/home/bernd", InetAddress.getByName("mail.g5r.eu"), 65432, RcStandard.ICWS88,
                Language.de);
        new SaveConfigFile(saveParams, berndCfgS, ConfigTypes.CLIENT_CONFIG);

        // read that file again
        LoadConfigFile loadCfg = new LoadConfigFile();
        loadCfg.loadClientPrefs(defaultCfgS, berndCfgS, ConfigTypes.CLIENT_CONFIG);
        ClientConfig loadCfgParams = (ClientConfig) loadCfg.getConfigParameters();

        assertEquals(1920, loadCfgParams.getResolutionWidth());
        assertEquals(1080, loadCfgParams.getResolutionHeight());
        assertEquals("Bernd Wiesacher", loadCfgParams.getWarriorName());
        assertEquals("/home/bernd", loadCfgParams.getPathToRcFile());
        assertEquals("188.68.54.64", loadCfgParams.getInetAddress().getHostAddress());
        assertEquals("mail.g5r.eu", loadCfgParams.getInetAddress().getHostName());
        assertEquals(65432, loadCfgParams.getPortLastServer());
        assertEquals("ICWS88", loadCfgParams.getRcStandard().toString());
        assertEquals("en", loadCfgParams.getLanguage().toString());
    }

    @Test
    public void testSetWidth_Error() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new ClientConfig(-5, 400, "bernd", System.getProperty("user.home"),
                InetAddress.getByName("localhost"), 4000, RcStandard.ICWS88, Language.de);
    }

    @Test
    public void testSetHeight_Error() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new ClientConfig(640, 0, "bernd", System.getProperty("user.home"),
                InetAddress.getByName("localhost"), 4000, RcStandard.ICWS88, Language.de);
    }

    @Test
    public void testSetName_Error() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new ClientConfig(640, 400, "", System.getProperty("user.home"),
                InetAddress.getByName("localhost"), 4000, RcStandard.ICWS88, Language.de);
    }

    @Test
    public void testSetPath_Error() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new ClientConfig(640, 400, "bernd", "", InetAddress.getByName("localhost"),
                4000, RcStandard.ICWS88, Language.de);
    }

    @Test
    public void testSetPort_Error1() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new ClientConfig(640, 400, "bernd", System.getProperty("user.home"),
                InetAddress.getByName("localhost"), -1, RcStandard.ICWS88, Language.de);
    }

    @Test
    public void testSetPort_Error2() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new ClientConfig(640, 400, "bernd", "", InetAddress.getByName("localhost"), 999999,
                RcStandard.ICWS88, Language.de);
    }

    @Test
    public void saveClientConfig_EmptyPath() throws IOException {
        thrown.expect(IllegalArgumentException.class);
        ClientConfig conf = new ClientConfig(640, 400, "bernd", System.getProperty("user.home"),
                InetAddress.getByName("localhost"), 1000, RcStandard.ICWS88, Language.de);
        new SaveConfigFile(conf, "", ConfigTypes.CLIENT_CONFIG);
    }
}