package gone.lib.network.serialization;

import gone.lib.common.ClientRole;
import gone.lib.common.NetworkConstants;
import gone.lib.common.RcStandard;

import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwMessageType;
import gone.lib.network.json.CwReadyContent;
import gone.lib.network.json.CwStartContent;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * this class
 *
 * @author winfried
 */
public class IncomingMessagesInterpreter {

    /**
     *
     * @param data
     * @return
     */
    public CwMessageType getMessageType(String data) {
        JSONObject object = new JSONObject(data);

        if (object.getString("packageType").equals("login")) {
            return CwMessageType.Login;
        } else if (object.getString("packageType").equals("start")) {
            return CwMessageType.Start;
        } else if (object.getString("packageType").equals("loginOk")) {
            return CwMessageType.LoginOk;
        } else if (object.getString("packageType").equals("ready")) {
            return CwMessageType.Ready;
        } else if (object.getString("packageType").equals("status")) {
            return CwMessageType.GameStatus;
        } else if (object.getString("packageType").equals("finish")) {
            return CwMessageType.GameResult;
        }
        return CwMessageType.Unknown;
    }

    public CwLoginContent parseLoginMessage(String json) {
        JSONObject object = new JSONObject(json);
        validate(object, "loginSchema.json");

        CwLoginContent result = new CwLoginContent();
        result.name = object.getJSONObject("content").getString("name");

        if (object.getJSONObject("content").getString("role").equals("Player")) {
            result.role = ClientRole.Player;
        } else if (object.getJSONObject("content").getString("standard").equals("Viewer")) {
            result.role = ClientRole.Viewer;
        }
        return result;
    }

    public CwStartContent parseStartMessage(String json) {
        JSONObject object = new JSONObject(json);
        validate(object, "startSchema.json");

        CwStartContent result = new CwStartContent();

        String[] fullStateArray = object.getJSONObject("content")
                .getString("fullState").split(NetworkConstants.FULLSTATESEPARATOR);
        List<String> fullStateList = new LinkedList<>();

        Collections.addAll(fullStateList, fullStateArray);

        result.fullState = fullStateList;
        result.players = object.getJSONObject("content").getInt("players");

        return result;
    }

    public CwLoginOkContent parseLoginOkMessage(String json) {
        JSONObject object = new JSONObject(json);
        validate(object, "loginOkSchema.json");

        CwLoginOkContent result = new CwLoginOkContent();
        result.clientMustWait = object.getJSONObject("content").getBoolean("clientMustWait");
        result.coreSize = object.getJSONObject("content").getInt("coreSize");
        result.lineLength = object.getJSONObject("content").getInt("lineLength");

        if (object.getJSONObject("content").getString("standard").equals("ICWS88")) {
            result.standard = RcStandard.ICWS88;
        } else if (object.getJSONObject("content").getString("standard").equals("ICWS94")) {
            result.standard = RcStandard.ICWS94;
        }
        return result;
    }

    public CwReadyContent parseReadyMessage(String json) {
        JSONObject object = new JSONObject(json);
        validate(object, "readySchema.json");

        CwReadyContent result = new CwReadyContent();

        String[] redcodeStatements = object.getJSONObject("content")
                .getString("warriorRedcode").split(NetworkConstants.REDCODESEPARATOR);
        List<String> redcodeStatementList = new LinkedList<>();

        Collections.addAll(redcodeStatementList, redcodeStatements);

        result.warriorRedcode = redcodeStatementList;

        return result;
    }

    public CwGameStatusContent parseGameStatusMessage(String json) {
        JSONObject object = new JSONObject(json);
        validate(object, "gameStatusSchema.json");

        CwGameStatusContent result = new CwGameStatusContent();
        result.playerOneActive = object.getJSONObject("content").getBoolean("playerOneActive");
        result.indexOfChangedCoreElement = object.getJSONObject("content").getInt("indexOfChangedCoreElement");
        result.newIpIndex = object.getJSONObject("content").getInt("newIpIndex");

        Object o = object.getJSONObject("content").get("newOpCode");
        if (o == null || o.equals(null)) {
            result.newOpCode = null;
        } else {
            result.newOpCode = object.getJSONObject("content").getString("newOpCode");
        }

        result.oldIpIndex = object.getJSONObject("content").getInt("oldIpIndex");

        return result;
    }

    public CwGameResultContent parseGameResultMessage(String json) {
        JSONObject object = new JSONObject(json);
        validate(object, "finishSchema.json");

        CwGameResultContent result = new CwGameResultContent();
        result.state = object.getJSONObject("content").getString("state");

        return result;
    }

    private void validate(JSONObject object, String name) {
        try (InputStream inputStream = getClass().getResourceAsStream(name)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema schema = SchemaLoader.load(rawSchema);
            schema.validate(object);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}