package gone.lib.network.contracts;

/**
 * Created by Winfried on 22.09.2016.
 */
public interface ITcpListener {
    void receiveMessage(String sessionId, String data);
}
