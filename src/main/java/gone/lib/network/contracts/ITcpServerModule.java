package gone.lib.network.contracts;

/**
 * Created by Winfried on 23.09.2016.
 */
public interface ITcpServerModule {
    void start(int serverPort);

    void sendMessage(String sessionId, String data);

    void registerListener(ITcpListener listener);

    void removeListener(ITcpListener listener);

    void stop();
}
