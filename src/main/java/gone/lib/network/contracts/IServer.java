package gone.lib.network.contracts;

import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwStartContent;

import javax.naming.OperationNotSupportedException;

/**
 * Created by winfr on 27.09.2016.
 */
public interface IServer {
    void start(int port) throws OperationNotSupportedException;

    void stop();

    void sendLoginOkTelegram(CwLoginOkContent telegram, String sessionId);

    void sendStartTelegram(CwStartContent telegram, String sessionId);

    void sendGameStatusTelegram(CwGameStatusContent telegram, String sessionId);

    void sendGameResultTelegram(CwGameResultContent telegram, String sessionId);

    boolean getIsRunning();
}
