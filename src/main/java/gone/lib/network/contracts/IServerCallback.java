package gone.lib.network.contracts;

import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwReadyContent;

/**
 * Created by winfr on 27.09.2016.
 */
public interface IServerCallback {
    void receiveLoginTelegram(CwLoginContent telegram, String sessionId);

    void receiveReadyTelegram(CwReadyContent telegram, String sessionId);
}
