package gone.lib.network;

import gone.lib.network.contracts.IServer;
import gone.lib.network.contracts.IServerCallback;
import gone.lib.network.contracts.ITcpListener;
import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwMessageType;
import gone.lib.network.json.CwStartContent;
import gone.lib.network.serialization.IncomingMessagesInterpreter;
import gone.lib.network.serialization.OutgoingMessagesSerializer;
import gone.lib.network.tcp.TcpServerModule;

import javax.naming.OperationNotSupportedException;

/**
 * Created by winfr on 27.09.2016.
 */
public class CwServer implements IServer, ITcpListener {
    private IServerCallback callback;
    private TcpServerModule server;
    private final OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();
    private final IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();

    private boolean running = false;

    public CwServer(IServerCallback callback) {
        this.callback = callback;
    }

    @Override
    public void start(int port) throws OperationNotSupportedException {
        if (running) {
            throw new OperationNotSupportedException("already running");
        }

        server = new TcpServerModule();
        server.registerListener(this);
        server.start(port);

        running = true;
    }

    @Override
    public void stop() {
        server.stop();
        running = false;
    }

    @Override
    public void sendLoginOkTelegram(CwLoginOkContent telegram, String sessionId) {
        server.sendMessage(sessionId, serializer.serializeLoginOkMessage(telegram));
    }

    @Override
    public void sendStartTelegram(CwStartContent telegram, String sessionId) {
        server.sendMessage(sessionId, serializer.serializeStartMessage(telegram));
    }

    @Override
    public void sendGameStatusTelegram(CwGameStatusContent telegram, String sessionId) {
        server.sendMessage(sessionId, serializer.serializeGameStatusMessage(telegram));
    }

    @Override
    public void sendGameResultTelegram(CwGameResultContent telegram, String sessionId) {
        server.sendMessage(sessionId, serializer.serializeGameResultMessage(telegram));
    }

    @Override
    public boolean getIsRunning() {
        return running;
    }

    @Override
    public void receiveMessage(String sessionId, String data) {
        CwMessageType type = interpreter.getMessageType(data);

        if (type == CwMessageType.Login) {
            callback.receiveLoginTelegram(interpreter.parseLoginMessage(data), sessionId);
        } else if (type == CwMessageType.Ready) {
            callback.receiveReadyTelegram(interpreter.parseReadyMessage(data), sessionId);
        } else {
            // TODO: print that to log file
            System.out.print("unknown or invalid message type");
        }
    }
}