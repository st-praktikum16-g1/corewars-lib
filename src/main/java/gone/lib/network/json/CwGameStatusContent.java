package gone.lib.network.json;

/**
 * message type that will be sent regularly from the server
 * to the client, which represents the game update of one
 * single game step at the server
 *
 * @author winfried
 */
public class CwGameStatusContent {
    public int indexOfChangedCoreElement;
    public String newOpCode;

    public int oldIpIndex;
    public int newIpIndex;
    public boolean playerOneActive;
}