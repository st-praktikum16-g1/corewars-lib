package gone.lib.network.json;

/**
 * collection of the different message types including an
 * unknown message type to deal with errors
 *
 * @author winfried
 */
public enum CwMessageType {
    Login,
    LoginOk,
    Ready,
    Start,
    GameStatus,
    GameResult,
    Unknown
}
