package gone.lib.network;

import gone.lib.network.contracts.IClient;
import gone.lib.network.contracts.IClientCallback;
import gone.lib.network.contracts.ITcpListener;
import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwMessageType;
import gone.lib.network.json.CwReadyContent;
import gone.lib.network.serialization.IncomingMessagesInterpreter;
import gone.lib.network.serialization.OutgoingMessagesSerializer;
import gone.lib.network.tcp.TcpClientModule;

import javax.naming.OperationNotSupportedException;

/**
 * Created by winfr on 27.09.2016.
 */
public class CwClient implements IClient, ITcpListener {
    private IClientCallback callback;
    private TcpClientModule client;
    private final OutgoingMessagesSerializer serializer = new OutgoingMessagesSerializer();
    private final IncomingMessagesInterpreter interpreter = new IncomingMessagesInterpreter();

    private boolean connected = false;

    public CwClient(IClientCallback callback) {
        this.callback = callback;
    }

    @Override
    public void connect(String address, int port) throws OperationNotSupportedException {
        if (connected) {
            throw new OperationNotSupportedException("already connected");
        }

        client = new TcpClientModule();
        client.registerListener(this);
        client.connect(address, port);

        connected = true;
    }

    @Override
    public void disconnect() {
        client.disconnect();
        connected = false;
    }

    @Override
    public void sendLoginTelegram(CwLoginContent telegram) {
        client.sendMessage(serializer.serializeLoginMessage(telegram));
    }

    @Override
    public void sendReadyTelegram(CwReadyContent telegram) {
        client.sendMessage(serializer.serializeReadyMessage(telegram));
    }

    @Override
    public IClientCallback getCallback() {
        return callback;
    }

    @Override
    public boolean getIsConnected() {
        return connected;
    }

    @Override
    public void receiveMessage(String sessionId, String data) {
        CwMessageType type = interpreter.getMessageType(data);

        if (type == CwMessageType.LoginOk) {
            callback.receiveLoginOkTelegram(interpreter.parseLoginOkMessage(data));
        } else if (type == CwMessageType.Start) {
            callback.receiveStartTelegram(interpreter.parseStartMessage(data));
        } else if (type == CwMessageType.GameStatus) {
            callback.receiveGameStatusTelegram(interpreter.parseGameStatusMessage(data));
        } else if (type == CwMessageType.GameResult) {
            callback.receiveGameResultTelegram(interpreter.parseGameResultMessage(data));
        } else {
            // TODO: print that to log file
            System.out.print("unknown or invalid message type");
        }
    }
}
