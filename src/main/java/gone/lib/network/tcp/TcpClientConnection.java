package gone.lib.network.tcp;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by winfr on 26.08.2016.
 * Represents an active connection to a client
 */
public class TcpClientConnection {
    /**
     * Buffered reader.
     */
    public DataInputStream input;

    /**
     * Buffered writer.
     */
    public DataOutputStream output;

    /**
     * Buffered writer.
     */
    public Socket clientSocket;
}
