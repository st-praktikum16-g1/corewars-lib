package gone.lib.network.tcp;

import gone.lib.common.NetworkConstants;
import gone.lib.network.contracts.ITcpListener;
import gone.lib.network.contracts.ITcpServerAccepted;
import gone.lib.network.contracts.ITcpServerModule;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Winfried on 23.09.2016.
 */
public class TcpServerModule implements ITcpServerModule, ITcpServerAccepted, ITcpListener {
    private List<ITcpListener> listeners = new LinkedList<>();
    private TcpServerAccept tcpServerAccept;

    /**
     * Starts the execution and waits for clients to connect.
     */
    @Override
    public void start(int serverPort) {
        tcpServerAccept = new TcpServerAccept(serverPort, this);
        new Thread(tcpServerAccept).start();
    }

    @Override
    public void stop() {
        tcpServerAccept.stop();
    }

    /**
     * Sends data to a client and optionally waits for a response.
     * @param sessionId the id for the respective client connection
     * @param data data to send
     */
    @Override
    public void sendMessage(String sessionId, String data) {
        TcpClientConnection connection = tcpServerAccept.clientConnections.get(sessionId);

        try {
            connection.output.writeUTF(data);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void acceptedConnection(String sessionId) {
        TcpReceiveMessage receiveMessage = new TcpReceiveMessage(
                tcpServerAccept.clientConnections.get(sessionId).input, sessionId, false);
        receiveMessage.addListener(this);
        Thread receiveThread = new Thread(receiveMessage);

        receiveThread.start();
    }

    /**
     * Registers a listener to be notified on incoming client messages.
     * @param listener a new listener
     */
    @Override
    public void registerListener(ITcpListener listener) {
        listeners.add(listener);
    }

    /**
     * Ends notifications for a listener.
     * @param listener a registered listener
     */
    @Override
    public void removeListener(ITcpListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void receiveMessage(String sessionId, String data) {
        for (ITcpListener listener: listeners) {
            listener.receiveMessage(sessionId, data);
        }
    }
}