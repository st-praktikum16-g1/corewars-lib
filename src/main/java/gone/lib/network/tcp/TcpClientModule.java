package gone.lib.network.tcp;

import gone.lib.common.NetworkConstants;
import gone.lib.network.contracts.ITcpClientModule;
import gone.lib.network.contracts.ITcpListener;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by winfr on 26.09.2016.
 */
public class TcpClientModule implements ITcpClientModule, ITcpListener {
    private DataOutputStream outToServer;
    private DataInputStream inFromServer;
    private Socket clientSocket;
    private List<ITcpListener> listeners = new LinkedList<>();

    /**
     * connects to a remote server
     * @param address
     * @param port
     */
    @Override
    public void connect(String address, int port) {
        try {
            clientSocket = new Socket(address, port);

            outToServer = new DataOutputStream(clientSocket.getOutputStream());
            inFromServer = new DataInputStream(clientSocket.getInputStream());

            TcpReceiveMessage receiveMessage = new TcpReceiveMessage(inFromServer, "server", false);
            receiveMessage.addListener(this);
            Thread receiveThread = new Thread(receiveMessage);

            receiveThread.start();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void sendMessage(String data) {
        try {
            outToServer.writeUTF(data);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void disconnect() {
        try {
            clientSocket.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void registerListener(ITcpListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(ITcpListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void receiveMessage(String sessionId, String data) {
        for (ITcpListener listener: listeners) {
            listener.receiveMessage(sessionId, data);
        }
    }
}