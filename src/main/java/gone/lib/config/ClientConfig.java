package gone.lib.config;

import gone.lib.common.Language;
import gone.lib.common.RcStandard;

import java.net.InetAddress;

/**
 * dataType which represents all configurable parameters for the client/ui
 * @author matthias
 */
public class ClientConfig extends ConfigParameters {
    private int resolutionWidth;
    private int resolutionHeight;
    private String warriorName;
    private String relativePathToRcFile;
    private InetAddress inetAddress;
    private int portLastServer;
    private RcStandard rcStandard;
    private Language language;

    /* constructor */

    /**
     * constructor
     * @param width the width of the active window (ui)
     * @param height the height of the active window (ui)
     * @param warriorName the name of the warrior
     * @param pathToRcFile the default path to the redcode file
     * @param inetAddress the last used server address
     * @param portLastServer the port that fits to that server address
     * @param rcStandard the last used rc standard
     * @param language the last used language
     */
    public ClientConfig(int width, int height, String warriorName, String pathToRcFile,
                        InetAddress inetAddress, int portLastServer, RcStandard rcStandard,
                        Language language) {
        setResolutionWidth(width);
        setResolutionHeight(height);
        setWarriorName(warriorName);
        setPathToRcFile(pathToRcFile);
        setInetAddress(inetAddress);
        setPortLastServer(portLastServer);
        setRcStandard(rcStandard);
        setLanguage(language);
    }

    /* setter */

    private void setResolutionWidth(int value) throws IllegalArgumentException {
        if (value < 360) {
            throw new IllegalArgumentException("width cant be < 360");
        }
        this.resolutionWidth = value;
    }

    private void setResolutionHeight(int value) {
        if (value < 300) {
            throw new IllegalArgumentException("height cant be < 300");
        }
        this.resolutionHeight = value;
    }

    private void setWarriorName(String value) throws IllegalArgumentException {
        if (value.isEmpty()) {
            throw new IllegalArgumentException("name cant be empty");
        }
        this.warriorName = value;
    }

    private void setPathToRcFile(String value) throws IllegalArgumentException {
        if (value.isEmpty()) {
            throw new IllegalArgumentException("path cant be empty");
        }
        this.relativePathToRcFile = value;
    }

    private void setInetAddress(InetAddress value) throws IllegalArgumentException {
        // TODO value check
        this.inetAddress = value;
    }

    private void setPortLastServer(int value) throws IllegalArgumentException {
        if (value < 0  || value > 65535) {
            throw new IllegalArgumentException("port must be >= 0 && <= 65535");
        }
        this.portLastServer = value;
    }

    private void setRcStandard(RcStandard value) throws IllegalArgumentException  {
        this.rcStandard = value;
    }

    private void setLanguage(Language value) {
        this.language = value;
    }

    /* getter */

    public int getPortLastServer() {
        return portLastServer;
    }

    public int getResolutionWidth() {
        return resolutionWidth;
    }

    public int getResolutionHeight() {
        return resolutionHeight;
    }

    public String getWarriorName() {
        return warriorName;
    }

    public String getPathToRcFile() {
        return relativePathToRcFile;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public RcStandard getRcStandard() {
        return rcStandard;
    }

    public Language getLanguage() {
        return language;
    }
}
