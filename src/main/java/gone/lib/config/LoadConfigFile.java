package gone.lib.config;

import gone.lib.common.Language;
import gone.lib.common.RcStandard;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

/**
 * loads the default/user config file (client, server)
 * the config file is stored as a preferences file (xml key=value, utf8)
 * xml dtd used: http://java.sun.com/dtd/preferences.dtd
 *
 * @author matthias
 */
public class LoadConfigFile {
    private ConfigParameters configParameters;

    /* constructor */

    /**
     * empty constructor, because we set the ConfigParameters variable
     * later via a call to either loadClientPrefs or loadServerPrefs
     */
    public LoadConfigFile() {
        this.configParameters = new ConfigParameters();
    }

    /* getter */

    public ConfigParameters getConfigParameters() throws IllegalAccessException {
        return configParameters;
    }

    /* class methods */

    /**
     * loads a client config pref file, transfer it into a intern ClientConfig object
     * @param defaultFilePath location of the default config file
     * @param userFilePath location of the user config file
     * @param configTypes type of the config to be loaded
     * @throws IOException if the file is not found
     * @throws InvalidPreferencesFormatException preferences are not formatted correct (xml dtd)
     */
    public void loadClientPrefs(String defaultFilePath, String userFilePath, ConfigTypes configTypes)
            throws IOException, InvalidPreferencesFormatException {

        String nodeName = configTypes.toString();

        // create and load default prefs
        InputStream in1 = new BufferedInputStream(new FileInputStream(defaultFilePath));
        Preferences.importPreferences(in1);
        Preferences defaultPrefs = Preferences.userRoot().node(nodeName);
        in1.close();

        // initiate user prefs with default prefs, load prefs
        Preferences userPrefs = defaultPrefs;
        InputStream in2 = new BufferedInputStream(new FileInputStream(userFilePath));
        Preferences.importPreferences(in2);
        in2.close();

        // get the pref values from userPrefs, set them to configParameters variable
        // rc standard, language
        RcStandard rcStd = (userPrefs.get("RC_STANDARD", RcStandard.ICWS88.toString()))
                .equals("ICWS88") ? RcStandard.ICWS88 : RcStandard.ICWS94;
        Language defLang = (userPrefs.get("LANGUAGE", Language.de.toString()))
                .equals("de") ? Language.de : Language.en;

        configParameters = new ClientConfig(userPrefs.getInt("WIDTH", 800),
                userPrefs.getInt("HEIGHT", 600), userPrefs.get("WARRIOR_NAME", "unknown warrior"),
                userPrefs.get("PATH_TO_RC_FILE", System.getProperty("user.home")),
                InetAddress.getByName(userPrefs.get("SERVER_IP", "localhost")),
                userPrefs.getInt("SERVER_PORT", 65500), rcStd, defLang);
    }

    /**
     * loads a server config pref file, transfer it into a intern ClientConfig object
     * @param path location of the config file
     * @param configTypes type of the config to be loaded
     * @throws IOException if the file is not found
     * @throws InvalidPreferencesFormatException preferences are not formatted correct
     */
    public void loadServerPrefs(String path, ConfigTypes configTypes)
            throws IOException, InvalidPreferencesFormatException {

        String nodeName = configTypes.toString();

        // create and load default prefs
        InputStream in = new BufferedInputStream(new FileInputStream(path));
        Preferences.importPreferences(in);
        Preferences defaultPrefs = Preferences.userRoot().node(nodeName);
        in.close();

        configParameters = new ServerConfig(
                defaultPrefs.getInt("MAX_CYCLES", 100000),
                defaultPrefs.getInt("MAX_START_INSTRUCTIONS_COUNT", 1000),
                defaultPrefs.getInt("PORT", 123),
                defaultPrefs.getInt("MAX_PLAYER_COUNT", 2),
                defaultPrefs.getInt("CORE_SIZE", 10000),
                defaultPrefs.getInt("WAIT_TIME_MS", 100));
    }
}
