package gone.lib.common;

/**
 * enum to store the different roles clients can have
 *
 * @author winfried
 */
public enum ClientRole {
    Player,
    Viewer
}